SHELL = /bin/bash

CWARNINGS = -Wall -Wextra -Wno-error -Wno-int-conversion -Wno-builtin-declaration-mismatch -Wno-main

ARCH = riscv64-elf
CFLAGS = -nostdlib -Os -g3 -ffreestanding $(CWARNINGS) -mcmodel=medany -I src
SOURCES = src/{entrypoint.S,spikeio.S,math.S,str.S}

out/:
	@mkdir $@

out/link.ld: link.ld out/
	@$(ARCH)-gcc -E -P - < $< > $@

out/link-test.ld: link.ld out/
	@$(ARCH)-gcc -D TEST -E -P - < $< > $@

out/asmcalc: out/link.ld
	@$(ARCH)-gcc         $(CFLAGS) -o $@ -T $< $(SOURCES)  src/main.S
out/asmcalc-test: out/link-test.ld
	@$(ARCH)-gcc -D TEST $(CFLAGS) -o $@ -T $< $(SOURCES) test/*.c

build: out/asmcalc
build-test: out/asmcalc-test

compile-test: out/link.ld
	@$(ARCH)-gcc -c -D TEST $(CFLAGS) -dumpdir out -T out/link-test.ld $(SOURCES) test/*.c

run: out/asmcalc
	@spike $<
run-test: out/asmcalc-test
	@spike $<
test: run-test

dis: out/asmcalc
	@$(ARCH)-objdump -d $<
dis-test: out/asmcalc-test
	@$(ARCH)-objdump -d $<

gdb: out/asmcalc
	@./gdb.sh $<
gdb-test: out/asmcalc-test
	@./gdb.sh $<

clean:
	@rm -rf out
re: clean

help:
	@printf "Targets:\n    run[-test] Run in emulator.\n    test       Same as run-test.\n    dis[-test] Disassemble built binary.\n    gdb[-test] Run in emulator with gdb attached.\n    clean      Remove binary files.\n    re         Same as clean.\n"
