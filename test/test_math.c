#include "assert.h"
#include "extern.h"

static char test_log10() {
    test_case("log10");
    int n = 1;
    for (int i = 1; i < 7; i++) {
        if (assert_equal("", log10(n), i))
            return finish_case(1);
        n *= 10;
    }
    return finish_case(0);
}

char test_math() {
    char failed = test_case("math");
    RUN_TEST(log10);
    return finish_case(failed);
}
