#include "sys.in"
#include "extern.h"
#include "assert.h"

word syscall(word code, word a1, word a2, word a3, word a4, word a5, word a6, word a7) {
    return raw_syscall(code, a1, a2, a3, a4, a5, a6, a7);
}

void print(char *buf, word len) {
    raw_syscall(sys_write, 1, buf, len, 0, 0, 0, 0);
}

// simple unoptimized strlen implementation
// may be more reliable than assembly strlen implementation
word cstrlen(char *buf) {
    word i = 0;
    while (1) {
        if (buf[i] == 0)
            return i;
        i++;
    }
    return i;
}

char test_str();
char test_math();

int main() {
    char failed = 0;
    RUN_TEST(str);
    RUN_TEST(math);
    return failed;
}
