#include "assert.h"
#include "extern.h"
#include "main.h"

static char test_strlen() {
    test_case("strlen");
    char *s = "012345";
    for (int i = 6; i > 0; i--) {
        s[i] = 0;
        if (assert_equal("", strlen(s), i))
            return finish_case(1);
    }
    return finish_case(0);
}

char test_str() {
    char failed = test_case("str");
    RUN_TEST(strlen);
    return finish_case(failed);
}