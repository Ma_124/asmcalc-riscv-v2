#ifndef EXTERN_H
#define EXTERN_H

#define word __INTPTR_TYPE__
#define uword __UINTPTR_TYPE__

// math.S
extern word log10(word);

// str.S
extern word strlen(char*);

// strint.S
extern word uitoa(char* start, char* end, word n);

// spikeio.S
#ifndef TEST
extern word syscall(word code, word a1, word a2, word a3, word a4, word a5, word a6, word a7);
#else
extern word raw_syscall(word code, word a1, word a2, word a3, word a4, word a5, word a6, word a7);
#endif

extern void exit(word code);
extern void quit();

#endif // EXTERN_H
