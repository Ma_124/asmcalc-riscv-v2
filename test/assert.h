#ifndef ASSERT_H
#define ASSERT_H

#define RUN_TEST(name) if (test_ ##name ()) failed = 1

char test_case(char* name);
char finish_case(char failed);

char assert_equal(char* msg, int a, int b);

#endif // ASSERT_H