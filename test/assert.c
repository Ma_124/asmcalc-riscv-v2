#include "main.h"
#include "extern.h"

int case_level = 0;
char verbose = 0;

void print_test_indent() {
    for (int i = 0; i < case_level; i++)
        print("  ", 2);
}

char test_case(char* name) {
    print_test_indent();
    case_level++;
    print("== ", 3);
    print(name, cstrlen(name));
    print("\n", 1);
    return 0;
}

char finish_case(char failed) {
    print_test_indent();
    case_level--;
    if (failed)
        print("FAIL\n", 5);
    else
        print("PASS\n", 5);
    return failed;
}

char assert_equal(char* msg, int a, int b) {
    if (a == b) {
        if (verbose) {
            print_test_indent();
            print(msg, cstrlen(msg));
            print("equal ", 6);
            // TODO print(itoa(a))
            print("\n", 1);
        }
        return 0;
    }

    print_test_indent();
    print(msg, cstrlen(msg));
    print("not equal ", 10);
    // TODO print(itoa(a))
    print(", ", 2);
    // TODO print(itoa(a))
    print("\n", 1);
    return 1;
}
