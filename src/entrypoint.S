#include "macro.in"

// Entrypoint of the executable.
//
// Note:
//   Never returns.
pub(_start):
    li sp, 0x80002000

    jal main
    // main returns the exit code in
    // a0 which is then passed to exit 
    jal exit

