.text

#include "macro.in"

// Write integer as base10 to buffer.
//
//
// Parameters:
//   a0 - Start of buffer.
//   a1 - End of buffer. This function will stop before writing to this address.
//   a2 - Unsigned integer to convert.
//
// Returns:
//   a0 - Address after the last character written by this function. Will be at most a1.
//
// Registers used:
pub(uitoa):
    ble a1, a0, 2f // end <= start

    // get number of digits
    push s0
    push ra
    mv s0, a0
    mv a0, a2

    call log10

    mv t0, s0
    pop s0
    pop ra

    // a0 - log10
    // t0 - start
    // a1 - end
    // a2 - num

    add a0, t0, a0
    // a0 - end of number

    // check that it fits into the buffer
    bge a0, a1, 3f

    // TODO

1:
    j 1b

2:
    ret

3:
    // TODO overflow strcopy("@OVERFLOW@", min(len(str), end-start)
    mv a0, t0
    ret
