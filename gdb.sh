#!/bin/bash

if [[ -z "$1" || "$1" == "--help" ]]; then
    echo This script is only supposed to be called by make
    exit 1
fi

cleanup() {
    kill "$SPIKE_PID" "$OPENOCD_PID"
    exit 1
}
trap cleanup INT

nohup spike -H --rbb-port=9824 "$1"        < /dev/null &> /dev/null &
SPIKE_PID="$!"
sleep 1
nohup riscv-openocd -f spike.cfg           < /dev/null &> /dev/null &
OPENOCD_PID="$!"
sleep 1
riscv64-elf-gdb -ex 'target remote localhost:3333' -x .gdbenv "$1"
cleanup

