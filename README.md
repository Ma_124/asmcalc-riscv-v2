# RISC-V Assembly Calculator v2
This repository contains a simple calculator written in RISC-V assembly.
Not because it's really useful or significantly faster than a normal one
but because it's a great exercise for learning assembly.

## Run
```bash
# Run the calculator
make run

# Run it's unit tests
make test

# Debug it in GDB
make gdb
```

## Dependencies
On Archlinux:
```bash
# For building and running:
pacman -S make riscv64-elf-gcc spike

# For debugging using GDB (yay being my AUR helper of choice):
pacman -S riscv64-elf-gdb
yay    -S riscv-openocd

# Or with one command:
yay -S make riscv64-elf-gcc spike riscv64-elf-gdb riscv-openocd
```

## License
Copyright 2020 Ma_124 <ma_124+oss@pm.me>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[`src/spikeio.S`] is based off [jonesinator/riscv-spike-minimal-assembly](https://github.com/jonesinator/riscv-spike-minimal-assembly) which is in the [public domain](https://github.com/jonesinator/riscv-spike-minimal-assembly/blob/master/UNLICENSE).

